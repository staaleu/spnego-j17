## Spnego Java-17 & Jakarta

Fork of https://spnego.sourceforge.net/ upgraded to use Java 17 and Jakarta EE. Minimal
changes made besides converting to Gradle, and upgrading to Java 17.
